# !/usr/bin/env python
# -*- coding:utf-8 -*-


"""
    RFM分析: 売上データを読み込み,RFM分析により顧客ごとのスコアを出力する
        入力: 月毎の売上データ(csv形式)
            index: ID
            column: 日付
        出力: RFMのランク及びトータルランク
            recency: 実行月日と直近購買月日の日差
            frequency: 契約回数
            monetary: 売上合計額
            R: 閾値を基準とする1〜9までの整数
            F: 閾値を基準とする1〜9までの整数
            M: 閾値を基準とする1〜9までの整数
            rfm_total_rank: R,F,Mの値を合計した値
    python --version: Python 3.7.2
    created: 2019-08-01
    Auther: S.TAKEHARA
"""

import pandas as pd
import numpy as np
import datetime as dt


def _read_csv():
    # 元データ読み込み
    df = pd.read_csv('sales.csv', comment='#')
    # カラム名変更
    df = df.rename(columns={'Unnamed: 0': 'uid'})
    # 欠損値を0に置換
    df = df.fillna(0)
    return df


# Recency：直近購買月
def _recency(df):
    # 0以外の値をカウントするためdfを転置
    df_t = df.T
    user_amount_list = []
    for i in range(len(df)):
        df_u = df_t.iloc[1:, i:i + 1]
        # indexの内容をカラムに持たせる
        df_u = df_u.reset_index()
        # カラム名付与
        df_u.columns = ['date', 'amount']
        # dateカラムをdatetime型に変換
        df_u['date'] = pd.to_datetime(df_u['date'], format='%Y/%m/%d')
        # amountカラムをint型に変換
        df_u['amount'].astype(np.int64)
        # dateで降順に並び替え
        df_u = df_u.sort_values('date', ascending=False)
        # 辞書型に変換
        df_u_dic = df_u.to_dict()
        # 辞書型を追加
        user_amount_list.append(df_u_dic)


    latest_contract_list = []
    for i in range(len(df)):
        df_ = pd.DataFrame(user_amount_list[i])

        # 直近購買月
        count = len(df_)
        for idx, j in enumerate(range(len(df_))):
            # print(type(df_.iloc[j, 1]))
            if df_.iloc[j, 1] < 0:
                pass
            elif df_.iloc[j, 1] == 0:
                count -= 1
                if count == 0:
                    latest_contract_list.append(0)
            elif df_.iloc[j, 1] > 0:
                # print(df_.iloc[j, 1])
                latest_contract_list.append(df_.iloc[j, 0])
                break

    # unixtimeに変換
    unixtime_list = []
    for time in latest_contract_list:
        try:
            unixtime = time.timestamp()
            unixtime_list.append(unixtime)
        except:
            unixtime_list.append(0)

    df_recency = pd.DataFrame(unixtime_list)
    # uid, total_monetary列を結合, カラム名0→recencyに変更
    df_recency = pd.concat([df["uid"], df_recency.rename(columns={0: 'recency'})], axis=1)
    # 集計日のtimestampを取得(ここでは2019/08/01)
    NOW = int(dt.datetime(2019, 8, 1).strftime('%s'))
    # uidを元にグループ化して、集計を行う
    df_recency = df_recency.groupby('uid').agg({'recency': lambda x: int((NOW - x.max()) / 86400)  # 最新購入日
    # 中央値
    time_median = df_recency['recency'].median()
    return df_recency, time_median


# Frequency：契約月数
def _frequency(df):
    # 0以外の値をカウントするためdfを転置
    df_zero = (df.T != 0)
    # 0行目(uid)を除く行において,0以外の個数(売上がある月)をカウント
    df_zero_count = df_zero.iloc[1:, :].sum()
    # カラムを追加
    df_frequency = pd.DataFrame(df_zero_count, columns=['frequency'])
    # uid, total_monetary列を結合
    df_frequency = pd.concat([df["uid"], df_frequency], axis=1)
    # print(df_frequency)
    # 中央値
    months_median = df_frequency['frequency'].median()
    return df_frequency, months_median


# Monetary：購買額
def _monetary(df):
    # 空のデータフレーム
    df_monetary = pd.DataFrame()
    # 顧客ごとの購入金額合計
    df_monetary['monetary'] = df.sum(axis=1)
    # uid, total_monetary列を結合
    df_monetary = pd.concat([df["uid"], df_monetary], axis=1)
    # 中央値
    amount_median = df_monetary['monetary'].median()
    # print(df_monetary)
    return df_monetary, amount_median


# uid,R,F,Mを結合
def _merge_rfm(df_recency, df_frequency, df_monetary):
    df_fm = pd.merge(df_frequency, df_monetary, on='uid')
    df_rfm = pd.merge(df_recency, df_fm, on='uid')
    print(df_rfm)
    return df_rfm


# ランク分け
def _divide_ranks(df_rfm, time_median, amount_median, months_median):
    ### Recency：直近購買月 ###
    # 閾値からの範囲
    recency_range = 10
    df_rfm.loc[df_rfm['recency'] >= time_median + recency_range, 'R'] = '1'
    df_rfm.loc[(df_rfm['recency'] >= time_median - recency_range) & (
            df_rfm['recency'] < time_median + recency_range), 'R'] = '2'
    df_rfm.loc[df_rfm['recency'] < time_median - recency_range, 'R'] = '3'

    ### Frequency：契約月数 ###
    # 閾値からの範囲
    frequency_range = 4
    df_rfm.loc[df_rfm['frequency'] >= months_median + frequency_range, 'F'] = '3'
    df_rfm.loc[(df_rfm['frequency'] >= months_median - frequency_range) & (
            df_rfm['frequency'] < months_median + frequency_range), 'F'] = '2'
    df_rfm.loc[df_rfm['frequency'] < months_median - frequency_range, 'F'] = '1'

    ### Monetary：購買額 ###
    # 閾値からの範囲
    amount_range = 200000
    # ランク分け
    df_rfm.loc[df_rfm['monetary'] >= amount_median + amount_range, 'M'] = '3'
    df_rfm.loc[(df_rfm['monetary'] >= amount_median - amount_range) & (
            df_rfm['monetary'] < amount_median + amount_range), 'M'] = '2'
    df_rfm.loc[df_rfm['monetary'] < amount_median - amount_range, 'M'] = '1'
    # csv出力
    df_rfm.to_csv("./rfm_test.csv")


def main():
    df = _read_csv()
    df_recency, time_median = _recency(df)
    df_monetary, amount_median = _monetary(df)
    df_frequency, months_median = _frequency(df)
    df_rfm = _merge_rfm(df_recency, df_frequency, df_monetary)
    _divide_ranks(df_rfm, time_median, amount_median, months_median)


if __name__ == '__main__':
    main()
